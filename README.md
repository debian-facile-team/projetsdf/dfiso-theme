# DFiso-Theme

Changeur de thème rapide pour DFiso/Xfce basé sur le HandyTheme

* sources : [HandyTheme par @prx](https://framagit.org/handylinux/debdev/-/blob/master/handylinuxlook/handylinuxlook-1.1/HandyTheme/handytheme.py)
* mise à jour python3: [par @Elzen](https://debian-facile.org/viewtopic.php?id=34488)
